# gnome-gmail

It allows desktop mail actions, such as 'Send File as Email' or web 'mailto' links, to be handled by the Gmail web client.

https://davesteele.github.io/gnome-gmail/index.html

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/gnome-packages/gnome-gmail.git
```

# IMPORTANT NOTE: Use viagee instead of gnome-gmail. As a part of a recent audit by Google, the branding for GNOME Gmail was changed. Note that OAuth identifies the service as "Viagee".
